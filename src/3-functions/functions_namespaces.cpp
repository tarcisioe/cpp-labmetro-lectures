#include <iostream>  // std::cout
#include <cmath>  // sqrt


// Funções e namespaces
// --------------------

// Para começarmos o assunto da aula de hoje veremos como formar a
// organização lógica do nosso código, utilizando namespaces.

// O que é um namespace? Um namespace nada mais é que uma coleção de entidades
// de um programa: funções, tipos, variáveis globais, etc. Já utilizamos
// diversas vezes o namespace mais conhecido: `std`, que engloba todos os
// elementos da biblioteca padrão, como `vector`, `string` e `cout`.

// Criar um namespace é bastante simples:

namespace math {

// Pronto! Tudo o que for declarado aqui dentro faz parte do namespace `math`,
// até o `}` final do namespace.

// Vamos então definir uma função dentro do nosso namespace. Como o nome que
// escolhemos é `math`, nossas funções farão diversas tarefas matemáticas.

double square(double n)
{
    return n*n;
}

// Vamos analisar cada linha:

// ```
//     tipo de retorno    nome do primeiro argumento
//    /                  /
// double square(double n)
//          \       \
//           nome   tipo do primeiro argumento
// ```

// Aqui temos a declaração da função. Nela definimos o seguinte:
// 1. O *tipo de retorno* da função. Toda função necessita retornar algo
//    de um tipo específico e predefinido.
// 2. O nome da função, para podermos utilizar ela depois.
// 3. Os argumentos da função. Todo possui um tipo, e pode opcionalmente
//    ter um nome. Se o argumento não for utilizado por algum motivo
//    (raro, mas pode ocorrer), não é necessário especificar o nome.

// *Importante*: Ao conjunto nome + tipos dos parâmetros damos o nome de
//               *assinatura da função*. Funções com assinaturas diferentes
//               são consideradas funções diferentes pelo compilador, mesmo
//               que seus nomes sejam iguais. A esse comportamento de
//               diferenciar funções levando em conta também o número de
//               argumentos e seus tipos damos o nome de *overload*, ou
//               *sobrecarga*.

// ```
//     return n*n;
// ```

// Como nossa função é bem curta, temos apenas o retorno de um valor: n
// multiplicado por ele mesmo, ou seja, n ao quadrado. Como veremos logo mais,
// dentro de uma função podemos utilizar tudo que já vimos até agora (`main`,
// onde estivemos programando até agora, de fato é uma função).

// Na primeira aula, construímos com um laço `while` o código necessário para
// aproximar numericamente a raiz quadrada de um número `n` qualquer.  Com
// funções, podemos encapsular essa operação para utilizá-la diversas vezes,
// sem a necessidade de escrever o laço novamente todas as vezes em que
// quisermos utilizá-la

double sqrt(double n)
{
    auto old = 0.0;
    auto guess = 1.0;

    while (old != guess) {
        old = guess;
        guess = (guess + n/guess)/2;
    }

    return guess;
}

// Recapitulando o que vimos antes: temos um parâmetro `n`, do tipo `double`.
// O tipo de retorno é `double`. A assinatura da função é

// sqrt(double)

// Lembrando sempre que o tipo de retorno não é considerado na assinatura
// da função: não é possível declarar duas funções com exatamente o mesmo
// nome e os mesmos tipos de parâmetro, mas que retornem tipos diferentes.

// Aqui temos mais um exemplo de função, dessa vez com dois parâmetros.  Ela
// calcula a exponenciação de um número por outro. O número a ser utilizado
// como base é um `double`, enquanto o expoente é um `unsigned long`, pois
// exponenciação por números negativos e reais não-inteiros é mais complexa do
// que o método que usaremos é capaz de calcular.

double power(double base, unsigned long exponent)
{
    auto answer = 1.0;

    for (auto i = 0u; i < exponent; ++i) {
        answer *= base;
    }

    return answer;
}

// A assinatura de `power` é
// power(double, unsigned long)

// Power utiliza elementos que já conhecemos: uma variável do tipo `double` para
// acumular o resultado, que é construido ao longo das execuções de um laço
// `for`. O laço for aplica a definição de exponenciação: para uma base e
// um expoente b^n, o resultado é uma multiplicação onde `b` aparece `n` vezes.

}

// Vamos introduzir agora um segundo namespace: isso nos dará a oportunidade de
// ver como os namespaces nos ajudam tanto a organizar nosso código quanto a
// resolver ambiguidades.

namespace recursive {

// Teremos uma outra implementação de `power` aqui. O objetivo de função é o
// mesmo: elevar uma base a um expoente. Nesta implementação, porém,
// utilizaremos a ideia de recursão: a possibilidade de uma função chamar ela
// mesma em sua definição.

double power(double base, unsigned long exponent)
{
    if (exponent == 0) {
        return 1;
    }

    auto half = recursive::power(base, exponent/2);

    auto result = half * half;

    if (exponent % 2 == 1) {
        result *= base;
    }

    return result;
}

// Nessa implementação de `power` a lógica é bastante diferente: em vez de
// executarmos diretamente a definição da exponenciação, basicamente tratamos
// três casos.

// O primeiro caso é o que chamamos de *caso base*: ele ocorre quando o
// expoente é 0, e nesse caso o resultado é sempre 1. Isso é tratado pelo
// primeiro `if` da função.

// Para entender os outros casos, vamos lembrar de uma propriedade da
// exponenciação: quando temos duas exponenciações, b^n e b^m, podemos
// escrever b^n * b^m como b^(n+m). Portanto, para qualquer expoente par
// n = 2m podemos escrever b^n = b^(2m) como b^m * b^m.

// E expoentes ímpares? Todo número ímpar pode ser escrito como 2m + 1,
// e 2^m é necessariamente par. Desta forma, podemos escrever b^n = b^(2n + 1)
// como b^n * b^n * b.

// Como a divisão inteira nos dá o resultado da divisão arrendondado para baixo,
// podemos sempre calcular b^(n/2). Se n for par, basta dobrar o resultado.
// Se n for ímpar, basta multiplicar isso mais uma vez pela base. Isso é tratado
// pelo segundo `if`.

// Esta segunda implementação é completamente diferente, mas calcula o mesmo
// resultado (salvo alguns possíveis erros introduzidos naturalmente pelo
// `double`). Veremos logo em seguida que ela também é bastante mais rápida.

}

int main()
{
    // Vamos demonstrar aqui a diferença entre as duas implementações de
    // `power`. Podemos fazer isso pois o namespace irá desambiguar, mesmo
    // que ambas as funções tenham o mesmo nome.

    auto a = math::power(1, 10000000000);
    std::cout << "23 ^ 4 = " << a << "\n";
    auto b = recursive::power(1, 10000000000);
    std::cout << "23 ^ 4 = " << b << "\n";

    // Elevamos 1 a um expoente grande para que não aconteça overflow. Note
    // a diferença de tempo entre a execução do primeiro e do segundo cálculos.
    // Isso ocorre porque a complexidade cai de linear no valor do expoente
    // para logarítmica no valor do expoente, mas não entraremos em detalhes
    // aqui sobre isso.

    // Em um escopo qualquer, podemos utilizar uma diretiva `using` para
    // buscar uma implementação de `power` de algum namespace específico.

    {
        using namespace math;
        std::cout << power(3, 7) << "\n";
    }

    {
        using namespace recursive;
        std::cout << power(3, 7) << "\n";
    }

    // Note que se utilizarmos a diretiva `using` em ambos os namespaces,
    // introduzimos uma ambiguidade que o compilador não consegue resolver.
    // Descomente o código a seguir para ver o efeito. Comente a terceira
    // linha do escopo para que ele volte a compilar.

    // {
    //     using namespace math;
    //     using namespace recursive;
    //     std::cout << power(3, 7) << "\n"; // Isso não compila
    //     std::cout << math::power(3, 7) << "\n"; // Isso compila
    // }

    return 0;
}
