Aula 3 - Funções e Namespaces
=============================

Bem vindos à terceira aula de C++! Nesta aula veremos funções, a primeira das
nossas ferramentas para tornar nosso código mais modular, legível e expressivo.
Veremos também namespaces, que são a principal ferramenta para organizar o
código em C++.


Seções
------

1. [Funções e namespaces](functions_namespaces.cpp)
2. [Código em vários arquivos - o *header*](functions.h)
3. [Código em vários arquivos - Implementação no *.cpp*](functions.cpp)
4. [Código em vários arquivos - Código cliente em *.cpp*](main.cpp)
5. [A compilação em várias partes](many_files.html)

Exercícios
----------

1. Escreva uma função que receba um caractere e uma `string` e retorne o número
   de vezes que o caractere aparece na string. **Lembre-se: copiar uma string
   pode ser custoso.** A string não deve ser alterada de forma alguma. Também
   lembre-se que é impossível um caractere aparecer um número negativo de
   vezes na string.

2. Escreva uma função que troca dois inteiros de lugar. Exemplo de uso:

    ```C++
    auto i = 2, j = 3;
    swap(i, j);
    std::cout << i == 3 << "\n"; // prints 1
    std::cout << j == 2 << "\n"; // prints 1
    ```

3. [Desafio] Escreva uma função que receba um vetor de inteiros e remova
   duplicatas, ou seja, sequências do mesmo valor, adjacentes.

    Exemplo:

    {1, 1, 1, 2, 3, 3, 4, 1, 1} -> {1, 2, 3, 4, 1}

    Retorne o resultado e não altere a entrada.

4. [Desafio] Faça o mesmo exercício que o acima, mas em vez de retornar o
   resultado, altere o vetor de entrada.

Apêndices
---------

### Apêndice 1 - Nomeando namespaces

Ao contrário de algumas outras linguagens, C++ não possui um padrão *de facto*
para nomenclatura de elementos do programa. O mais razoável, portanto, é sempre
ter um padrão simples a ser seguido no projeto todo, possivelmente tentando se
assemelhar às bibliotecas usadas. Namespaces, porém, são um dos elementos para
os quais mais se segue um padrão: eles costumam ser nomeados em letras
minúsculas, com underscores para separar palavras, como está sendo usado nessa
aula.

### Apêndice 2 - Parâmetros por valor e por referência

Como vimos nesta aula, é possível passar parâmetros para funções de duas formas
(de certa forma, três): por **valor** e por **referência**. O que isso
significa?

Passagem por **valor**: Uma cópia do valor é passada para a função, por
completo.  Isso pode vir a ser caro, porém: imagine copiar um vector com 1
milhão de elementos (isso é menos absurdo do que parece). É 1MB de memória
sendo copiado por inteiro. Por outro lado, fazendo isso temos garantia de que o
valor passado para a função não é modificado: se qualquer alteração no
parâmetro for feita dentro da função, apenas a cópia é alterada.

Passagem por **referência**: quando ocorre a passagem por referência (`&` antes
do nome do parâmetro) a função tem acesso direto ao objeto original. Isso
significa que não precisa haver uma cópia, o que é potencialmente mais
eficiente. Por outro lado, qualquer alteração no objeto acontece no objeto
original, o que normalmente é indesejável.

Mas e se quisermos algo eficiente e ao mesmo tempo protegido de alterações?
Existe solução: a referência constante. Quando um parâmetro é declarado como
`const T &` para um tipo `T` qualquer, vale a eficiência da referência, com a
segurança da cópia: uma referência constante não pode ser utilizada de forma a
alterar o objeto. Veremos mais sobre como isso é implementado por outros
ângulos na aula seguinte, mas por ora, não deixe de fazer os experimentos
apontados em `many_files/main.cpp`.

Por fim, duas regras a levar em consideração: não use referências constantes
para tipos primitivos (int, bool, double, etc.) pois copiá-los em geral é mais
eficiente que acessá-los pela referência ou igual. Só utilize referências para
eles se quiser alterar a variável original, o que deve ser raro.

A segunda regra é: se dentro de uma função acabarmos por copiar o valor
eventualmente, não é necessário receber por referência constante: receba o
valor por cópia e utilize o parâmetro como bem entender.
