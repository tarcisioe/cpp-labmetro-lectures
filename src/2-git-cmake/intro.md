O que é Git? E CMake?
=====================


Git
---

O Git é uma ferramenta de **controle de versão**, também chamadas SCM¹
ou VCS².

A principal característica do Git (e de qualquer outro software com propósito
semelhante) é a capacidade de registrar e armazenar o estado de um projeto em
um dado ponto no tempo. A este registro costuma-se dar o nome de **commit**.

<img class="figure" src="/static/images/git-commit-drawing.png" alt="Ilustração
de um commit no git."/>

Um commit no git costuma ser representado graficamente como um círculo com
setas chegando e saindo. Isso representa o fato de que todo commit tem um ou
mais pais e um ou mais filhos. Qualquer um dos **pais** de um commit
representam um estado cronologicamente **anterior** do projeto, enquanto
qualquer um dos **filhos** representa um estado **posterior**.

<img class="figure" src="/static/images/git-tree-drawing.png" alt="Ilustração
de uma árvore de commits."/>

Todo commit tem um ou mais commits pais, exceto por um especial: o primeiro.
Este commit costuma ser de uns poucos arquivos, quando um projeto já começa
versionado sob Git, ou os arquivos de uma versão estável de um projeto antigo
que começou a ser versionado como Git em um estado mais maduro. Como toda
alteração no projeto necessariamente parte de um estado anterior, esse é o
único commit que não tem nenhum pai.

Nada impede commits diferentes de terem o mesmo pai: isso ocorre quando duas
alterações independentes são feitas a partir de um mesmo estado. Um exemplo
simples é quando dois desenvolvedores implementam duas novas features ao mesmo
tempo. Chamamos isso de **branching**.

<img class="figure" src="/static/images/git-parent-drawing.png" alt="Ilustração
de um branch."/>

No geral, quando um branch é iniciado, espera-se que as alterações feitas nele
sejam integradas de volta ao branch principal do projeto (geralmente chamado
**master**). O Git auxilia nessa integração, e a esse processo damos o nome de
**merge**.

<img class="figure" src="/static/images/git-merge-drawing.png" alt="Ilustração
de um merge."/>

Mesmo com a explicação acima, a melhor maneira de entender como o Git funciona
é vendo ele em ação. Siga para a [Instalação do Git for Windows](git.html) para
poder começar.


CMake
-----

CMake é um utilitário para descrever a estrutura de um projeto em C ou C++. Com
ele, é possível de maneira genérica descrever um projeto que pode ser compilado
em diversas plataformas, e gerar diversas descrições de projetos, dentre elas
uma solution do Visual Studio 2017. O CMake permite que, com um arquivo simples
como

```cmake
cmake_minimum_required(VERSION 3.9)
project(Lecture1 CXX)

add_executable(hello
    main.cpp
)
```

Seja gerada uma solution como a que criamos na [Aula 1](../1-intro/vs.html).

Além de solutions pra diversas versões do Visual Studio, o CMake permite gerar
Makefiles simples para compilar em ambientes Unix, projetos do XCode para
compilar em MacOS X, entre diversos outros tipos de arquivo de projeto. Com
isso, desde que nosso código em C++ não seja estritamente dependente de uma
plataforma, o CMake nos dá o ferramental necessário para suportar diversas
plataformas desde o início.

Para seguir a instalação do CMake no Windows, siga para [Instalação do
CMake](cmake.html).


Notas
-----

**¹SCM**: _Source Code Manager_, ou gerenciador de código fonte. Este nome se
refere à capacidade de uma ferramenta como o Git (ou CVS, SVN, Mercurial...) de
manter sob controle um projeto (geralmente de software) que envolve um código
fonte, envolvendo controlar as várias versões, facilitar a integração de partes
desenvolvidas por programadores diferentes, etc.

**²VCS**: _Version Control System_, ou sistema de controle de versão. Este nome
se refere, em geral, ao mesmo tipo de ferramenta que "SCM", porém faz
referência direta à capacidade de controlar versões, ou seja, manter a
possibilidade de voltar o estado do código fonte como estava em um certo
momento anterior.
