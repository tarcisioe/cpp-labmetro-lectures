Aula 2 - Git e CMake
====================

Na aula de hoje veremos como criar um projeto utilizando CMake para gerar a
solution do Visual Studio e então como versioná-lo utilizando Git, e enviar
para um repositório remoto no [GitLab](https://gitlab.com).

Seções
------

Esta aula está dividida em seções, cada uma demonstrada em um trecho de
código com explicações em comentários.

0. [O que é Git? E CMake?](intro.html)
1. [Instalação do Git for Windows](git.html)
2. [Instalação do CMake](cmake.html)
3. [Criando um projeto no Visual Studio pelo CMake](cmake-project.html)
4. [Criando uma conta no GitLab](gitlab.html)
5. [Enviando um projeto ao GitLab com o git](git-remote.html)


Exercícios
----------

Para esta aula, o exercício é o que foi desenvolvido nos exercícios. O
resultado do exercício dessa aula é o repositório Git que foi enviado ao
GitLab (gitlab.com/[seu-usuario]/[nome-do-projeto]).

Referências
-----------

1. [Pro Git v2](https://git-scm.com/book/en/v2)
2. [CMake Documentation](https://cmake.org/cmake/help/latest/manual/cmake-language.7.html)
