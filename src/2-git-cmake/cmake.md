Instalação do CMake
===================

Para instalar o CMake faça o Download na [página](https://cmake.org/download/).
A versão relevante muito provavelmente (se o seu computador é mais recente que
2010) é a x64, ilustrada abaixo (o número do versão pode ser maior):

<img class="figure" src="/static/images/cmake-dl.png" alt="Link de download do
CMake." width="900"/>

Após iniciar a instalação, clique em **Next**, marque a caixa para **aceitar a
licença** e depois **Next** novamente. A próxima tela pede para você escolher
como o CMake deve alterar o PATH do sistema. Escolha a seguinte opção:

<img class="figure" src="/static/images/cmake-path.png" alt="Escolha de PATH do
CMake." width="500"/>

A próxima opção será onde o CMake deve ser instalado. Para a maior parte dos
usuários, o padrão é razoável. Clique em **Next** e depois em **Install** e o
resto da instalação correrá sozinho.

Se tudo foi feito corretamente e o Git já estiver instalado, abra uma pasta
qualquer, clique com o botão direito em um espaço vazio e escolha a opção **Git
Bash Here**. Na tela em que abrir, digite `cmake` e dê **Enter**. Se tudo
estiver correto, a saída será semelhante ao seguinte:

<img class="figure" src="/static/images/git-bash-cmake.png" alt="Escolha de
PATH do CMake." width="600"/>

Se tudo estiver correto, continue para [Criando um projeto no Visual Studio
pelo CMake](cmake-project.html).
