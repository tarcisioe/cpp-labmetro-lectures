Criando um projeto no Visual Studio pelo CMake
==============================================

Para criar um projeto utilizando o CMake iremos primeiro precisar de uma pasta
vazia. Crie uma no diretório em que desejar.

Crie um arquivo de texto vazio. A forma mais simples de fazer isso é clicando
com o botão direito em um espaço qualquer da pasta, escolhendo **New > Text
Document** e renomeando o arquivo que aparecer. Renomeie o arquivo para
`main.cpp`. É importante que a extensão mude de `txt` para `cpp`. Se as
extensões não estiverem aparecendo, habilite-as na parte de cima da janela
(**View > File name extensions**):

<img class="figure" src="/static/images/file-extensions.png" alt="Habilitar
mostrar extensões de arquivos." width="800"/>

Edite o arquivo `main.cpp` e insira nele o seguinte conteúdo:

```cpp
#include <iostream>

int main()
{
    std::cout << "I am a calculator!\n";
}
```

Depois disso, crie um arquivo de texto dentro da mesma pasta chamado
`CMakeLists.txt`. Neste arquivo insira o seguinte conteúdo:

```cmake
cmake_minimum_required(VERSION 3.9)
project(calc CXX)

add_executable(calc
    main.cpp
)
```

Com isso pronto, clique com o botão direito em alguma parte em branco da pasta
e escolha **Git Bash Here**. Na janela em que abrir, execute o seguinte comando:

```bash
mkdir build
```

Você perceberá que foi criada uma pasta `build` no diretório em que o Git Bash
foi aberto. Agora precisamos entrar nela com o Git Bash:

```bash
cd build
```

No navegador de arquivos, entre na pasta `build` para poder ver o que ocorre
ao rodar o CMake. Volte para o Git Bash e execute:

```bash
cmake ..
```

A saída deve ser semelhante à seguinte:

<img class="figure" src="/static/images/cmake-success-build.png" alt="Saída do
CMake ao gerar com sucesso." width="700"/>

Se isso correu como esperado, vá na pasta `build` com o gerenciador de arquivos
e irá encontrar vários arquivos, inclusive uma **solution** chamada `calc.sln`.
Abra ela no Visual Studio. Clique com o botão direito no projeto `calc`, selecione
**Set as StartUp Project** e aperte **Ctrl+F5**. Aceite que o projeto compile e
assista o programa ser compilado e na sequência executar.

Se tudo deu certo, é hora de começar a versionar o projeto com o Git. Volte para
o Git Bash e volte um diretório, se ainda estiver dentro de `build`:

```bash
cd ..
```

Para iniciar um repositório Git, execute `git init`.

Se você executar o seguinte comando, verá todos os arquivos da pasta que ainda
não foram versionados (ou seja, todos) em vermelho:

```
git status
```

<img class="figure" src="/static/images/git-status.png" alt="Git exibindo
arquivos não versionados." width="700"/>

O primeiro passo para criar um `commit` é definir o que será registrado nele.
Não queremos a pasta `build/`, pois ela é um produto da geração do projeto.
Então execute o seguinte:

```bash
git add CMakeLists.txt main.cpp
```

Se você executar `git status` novamente, verá em verde os arquivos que entrarão
em um commit se ele for realizado agora:

<img class="figure" src="/static/images/git-status-post-add.png" alt="Git
status após adicionar arquivos para serem commitados." width="700"/>

Para finalizar o `commit` execute

```bash
git commit -m "first commit"
```

... mas espere! Se você nunca utilizou o Git antes, nesse momento ele irá pedir
seu nome e-mail. Para configurar isso, execute o seguinte (mantenha as aspas):

```bash
git config --global user.name "Seu Nome Completo"
git config --global user.email "seu.email@gmail.com"
```

Depois disso, execute novamente `git commit -m "first commit"`. Tudo deve
correr bem dessa vez. Por enquanto ignoraremos a pasta `build` por nós mesmos,
mas logo mais veremos como fazer com que o `git` não mostre mais ela como
não-versionada.
