Instalação do Git for Windows
=============================

Neste curso utilizaremos o software **Git for Windows**. Para baixá-lo, siga o
link: [Git for Windows](https://git-for-windows.github.io/).

Na página principal, clique no botão de Download e (no Windows) o download
iniciará automaticamente:

<img class="figure" src="/static/images/git-dl.png" alt="Página de download do
Git for Windows." width="700"/>

Após o download do instalador, execute-o. Clique em **Next** para aceitar a
licença e o instalador irá exibir os componentes disponíveis para instalar.
Selecione no mínimo os que estão mostrados a seguir.

<img class="figure" src="/static/images/git-components.png" alt="Escolha dos
componentes a instalar." width="500"/>

Depois, o instalador irá mostrar opções relacionadas a alterar a variável
**PATH** do sistema.  Precisamos apenas do mínimo: poder utilizar o comando
`git` a partir da linha de comando do Windows, então escolha a opção do meio,
como abaixo.

<img class="figure" src="/static/images/git-path.png" alt="Escolha do que será
adicionado ao PATH." width="500"/>

O Git oferecerá a possibilidade de escolher entre a biblioteca de criptografia
aberta OpenSSL ou a biblioteca do Windows. Escolha o **OpenSSL**, como abaixo.

<img class="figure" src="/static/images/git-ssl.png" alt="Escolha de
implementação de criptografia." width="500"/>

Depois disso, será perguntado como deixar os caracteres de fim-de-linha nos
arquivos commitados. Este é um detalhe de como sistemas diferentes lidam com
arquivos de texto. Para garantir todas as compatibilidades, escolha a **primeira
opção**.

<img class="figure" src="/static/images/git-crlf.png" alt="Escolha de quebra de
linha." width="500"/>

Escolha a opção de utilizar o **MinTTY**, que é bastante mais agradável que o
Prompt de Comando do Windows.

<img class="figure" src="/static/images/git-terminal.png" alt="Escolha de
terminal para o Git." width="500"/>

Nas opções extras que aparecerem, selecione o seguinte:

<img class="figure" src="/static/images/git-extra.png" alt="Opções extras do
Git." width="500"/>

Depois disso, clicar em **Next** irá iniciar a instalação. Você pode escolher
**Launch Git Bash** se já desejar abrir um terminal com Git.
