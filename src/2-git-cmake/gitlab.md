Criando uma conta no GitLab
===========================

Para criar uma conta no GitLab acesse [o
site](https://gitlab.com/users/sign_in).  Escolha a aba **Register** e preencha
seus dados (no exemplo o usuário já está em uso):

<img class="figure" src="/static/images/gitlab-register.png"
alt="Formulário de registro do GitLab" width="400"/>

Você receberá um e-mail de confirmação no e-mail provido. Acesse-o e confirme
sua conta. Depois disso faça sign-in (pela mesma página linkada anteriormente).

<img class="figure" src="/static/images/gitlab-signin.png"
alt="Formulário de sign-in do GitLab" width="400"/>

Após autenticar, você verá uma página como a seguinte, mas sem projetos:

<img class="figure" src="/static/images/gitlab-projects.png"
alt="Página de projetos do GitLab" width="700"/>

Clique em **New Project** e você verá um formulário como o seguinte:

<img class="figure" src="/static/images/gitlab-new.png"
alt="Criação de novo projeto no GitLab" width="600"/>

Após clicar em **Create Project**, você verá uma página com instruções. As que
nos importam para nosso projeto são as em **Existing Git repository**:

```bash
cd existing_repo
git remote add origin git@gitlab.com:tarcisioe/cppcalc2.git
git push -u origin --all
git push -u origin --tags
```

Seguiremos essas instruções com detalhes em [Enviando um projeto ao GitLab com
o git](git-remote.html).
