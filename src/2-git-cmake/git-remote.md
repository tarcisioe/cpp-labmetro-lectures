Enviando um projeto ao GitLab com o Git
=======================================

Agora que já temos um commit no nosso projeto e um repositório remoto no
GitLab, veremos como enviar nosso trabalho.


Gerando e registrando um par de chaves
--------------------------------------

Se é a primeira vez que você está enviando algo ao GitLab, é interessante gerar
um par de chaves RSA. Isso facilita na hora de enviar commits, pois não é
necessário ficar inserindo usuário e senha toda vez.

Gerar a chave RSA é algo que só precisa ser feito **uma vez por computador**, ou
se você guardar sua chave privada de maneira segura e compartilhá-la entre PCs,
uma vez só.

Para gerar o par de chaves, execute:

```bash
ssh-keygen
```

Você verá as seguintes saídas:

```
Generating public/private rsa key pair.
Enter file in which to save the key (/home/<seu-usuario>/.ssh/id_rsa):
```

Basta apertar **Enter**, sem digitar mais nada, para salvar nesse mesmo lugar,
que é considerado padrão.

```
Enter passphrase (empty for no passphrase):
```

Se você digitar uma senha aqui, ela terá que ser digitada a cada `git push` ou
`git pull`. Se deixar sem senha, lembre-se de **guardar muito bem** o par de
chaves.

```
Enter same passphrase again:
```

Apenas digite a mesma senha e aperte enter.

Aprecie o randomart gerado por alguns segundos e execute

```
cat ~/.ssh/id_rsa.pub
```

Você verá algo no seguinte formato:

```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDY0DkF4vqdJ6pcMKzItrnZE42bRwVe2EwmQSdFbPmP
sDhbF1/o/wJPtBHwrDQzUXtUqw2aKYGzb/O9BcLkTyamsbMeYDxVwuP9C85tppolvPn+pENC3XjUxARx
ipzYf9sGSWTxISJCuVKUTtyQYPX7+IRUEKlqt791aIAAQstChi0B4bR7lvnqHLE9BfgUQ+QTopjgOmGW
XC9GzY0JbofYIkHQQmBvxUG3HA0LbI4Wo0CV2Mo5XwruUnlclyYZkqYQjtwHrTJ+MSmDQSrw4vKNoYyh
kxXY6GGRbKpLjg5x4gCC5Np8FqOzVgHeEkfrG1uIkWDH/2FtgAhnO/ULYWm9 tarcisioe@jinx
```

Esta é uma chave pública de verdade. Mas como ela é **pública**, não há nenhum
problema em estar disponível para qualquer pessoa ver. É da chave privada
(`~/.ssh/id_rsa`) que é necessário cuidar muito bem.

Copie o texto que saiu como output (no seu terminal, não o acima!). Vá ao site
do GitLab e clique na sua imagem/foto, à direita. Escolha **Settings** e vá em
**SSH Keys**. Cole o texto copiado no campo **Key** e clique em **Add Key**.
Pronto, sua chave pública está registrada no GitLab.


Adicionando o repositório remoto
--------------------------------

No Git Bash, navegue com `cd` até a pasta do seu projeto, onde você fez o commit,
e execute o seguinte comando:

```bash
git remote add origin git@gitlab.com:<seu-usuario>/<seu-projeto>.git
```

A linha acima pode ser copiada do que foi sugerido pelo GitLab na parte
anterior da aula.

Isso informa o Git de que o repositório no GitLab é um repositório remoto do mesmo
projeto, para onde podemos enviar commits (`push`) e de onde podemos buscar commits
(`pull`). O nome `origin` é arbitrário, mas é um padrão do Git (por exemplo, ao fazer
`clone` de um repositório, já se utiliza o nome `origin` para o remoto).

Depois disso, basta executar:

```bash
git push -u origin master
```

A opção `-u` serve para informar o Git que o branch `master` local e o remoto
são conectados, e que quando houver `push` ou `pull` eles devem ser atualizados
de acordo.

Depois disso, acesse seu projeto no GitLab e verá que todos seus arquivos estão
lá!  Parabéns, você tem um projeto publicado!
