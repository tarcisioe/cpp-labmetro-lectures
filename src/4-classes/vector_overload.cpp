#include "vector_overload.h"


namespace math {

Vector::Vector(double x, double y) :
    x_{x},
    y_{y}
{}

double Vector::x() const
{
    return x_;
}

double Vector::y() const
{
    return y_;
}

Vector& Vector::invert()
{
    return *this = Vector{-x_, -y_};
}

Vector &Vector::operator*=(double s)
{
    x_ *= s;
    y_ *= s;

    return *this;
}

Vector &Vector::operator+=(const Vector& other)
{
    x_ += other.x_;
    y_ += other.y_;

    return *this;
}

Vector operator+(Vector lhs, const Vector& rhs)
{
    lhs += rhs;
    return lhs;
}

Vector operator-(Vector v)
{
    v.invert();
    return v;
}

Vector operator*(Vector lhs, double s)
{
    lhs *= s;
    return lhs;
}

Vector operator*(double s, Vector rhs)
{
    return rhs * s;
}

}
