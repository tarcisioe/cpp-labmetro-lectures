#ifndef MATH_VECTOR_H
#define MATH_VECTOR_H

namespace math {

class Vector {
public:
    Vector(double x, double y);
    Vector() = default;

    double x() const;
    double y() const;

    Vector &invert();

    Vector &operator*=(double);
    Vector &operator+=(const Vector&);

    Vector(const Vector&) = default;

private:
    double x_{0}, y_{0};
};

Vector operator+(Vector, const Vector&);
Vector operator-(Vector);
Vector operator*(Vector, double);
Vector operator*(double, Vector);

}

#endif
