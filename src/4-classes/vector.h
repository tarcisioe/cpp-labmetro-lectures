#ifndef MATH_VECTOR_H
#define MATH_VECTOR_H

namespace math {

// Nosso primeiro exemplo de classe será um vetor. Atenção aqui: não estamos
// falando do vetor como o `std::vector`: aqui estamos falando do vetor que é
// usado em geometria para representar uma direção e um sentido.

// Escolhemos vetores como exemplo de classe pois ele é mais complexo que um
// tipo primitivo, mas mesmo assim possui operações simples para descrevermos.
// Ele permitirá explorar tudo que pretendemos explicar sobre classes nesta
// aula.

// Para criar uma classe utilizamos a keyword `class`. Uma classe introduz um
// tipo no programa, ou seja, após a definição da classe ela pode ser utilizada
// como um tipo como os que já vimos antes. Por exemplo, `std::string` é uma
// classe, apesar de sua definição ser um pouco mais complexa que a classe que
// mostraremos abaixo.

class Vector {

    // A partir daqui o tipo `math::Vector` já existe para o nosso programa. Um
    // tipo, por sua vez, possui uma **interface**. É importante, porém, que
    // cuidemos com essa palavra: interface é um termo com diversos
    // significados quando falamos de programação. O que queremos dizer aqui
    // é o seguinte: a interface de um tipo define como interagimos com
    // ele, ou seja, como o utilizamos. Como estamos introduzindo no programa
    // um tipo completamente novo, somos responsáveis por definir duas coisas:
    //     * Como ele funciona.
    //     * Como ele é utilizado.

    // Por termos a responsabilidade de definir estas duas coisas, C++ traz a
    // possibilidade de separar detalhes explicitamente, com o conceito de
    // `public` e `private`. Elementos internos do tipo que estamos definindo
    // (chamados **membros**) podem ser públicos, ou seja, acessíveis para quem
    // utiliza o tipo, ou privados, ou seja, acessíveis somente dentro da
    // definição interna do tipo. A isso damos o nome de **visibilidade**.

    // Em C++, membros `public` e `private` se definem de forma diferente de
    // outras linguagens como Java ou C#. O qualificador `public` (ou `private`)
    // define que tudo que vem após ele (e antes de um próximo qualificador)
    // possui aquela visibilidade. Notamos isso a seguir:

public:
    // Na seção `public` veremos todos os métodos da classe `Vector`. Os
    // *métodos* de uma classe são funções, semelhante às que vimos na aula 3,
    // mas diretamente ligadas aos objetos da classe. Vimos métodos em objetos
    // da clase `std::string` e `std::vector`, como `size()`, `empty()` e
    // `push_back()`. Métodos também são chamados *funções-membro*.

    // A primeira função pública importante que veremos nessa classe é o
    // **construtor**. O construtor define como um valor do tipo da classe é
    // inicializado.  Como as definições dos elementos da classe ficam no
    // arquivo `.cpp`, por enquanto só sabemos que o construtor aceita dois
    // `double`, `x` e `y`, e veremos sua implementação depois.

    Vector(double x, double y);

    // O(s) construtor(es) de uma classe são responsáveis por criar um valor
    // (chamado objeto) dessa classe de forma que ele esteja em um estado
    // válido e conhecido. A isto se faz referência em alguns lugares como
    // "iniciar as invariantes" de uma classe.

    // O construtor deve garantir que, após sua execução, todos os métodos de
    // uma classe tem condições de executar corretamente. Isso será feito
    // inicializando os valores dos membros internos da classe, que estão
    // declarados na seção `private`, ao final, como veremos adiante.

    // Classes também podem ter um construtor chamado "construtor padrão". O
    // construtor padrão é nada mais que um construtor que possa ser chamado
    // sem nenhum parâmetro. Para alguns tipos, isso pode fazer sentido,
    // enquanto para outros, não.

    // Para o tipo que estamos definindo, conseguimos pensar em um construtor
    // padrão bastante simples: o construtor padrão pode criar um vetor (0, 0).
    // Em outros tipos, o construtor padrão pode ser inviável: um exemplo
    // simples é uma classe representando uma pessoa qualquer, que tenha um
    // nome. Que nome seria dado a uma pessoa "padrão"? É impossível tomar
    // essa decisão sem escolher um valor completamente arbitrário. Portanto,
    // não se deve definir um construtor padrão.

    // Se nenhum construtor for declarado para uma classe, o compilador gera
    // um construtor padrão sempre que possível, simplesmente invocando a
    // construção padrão de cada um dos membros. Como nós já declaramos um
    // construtor, precisamos definir o construtor padrão nós mesmos se
    // quisermos. Temos duas formas de fazer isso: uma é definindo-o por
    // inteiro, como fizemos com o outro. A outra é solicitar ao compilador que
    // o gere. Como o que precisamos é exatamente do construtor padrão que
    // o compilador geraria, podemos fazer isso, da seguinte forma.

    Vector() = default;

    // Note que a linha acima é tanto uma declaração do construtor como uma
    // definição do construtor. Já se sabe, a partir dela, a implementação
    // do construtor padrão de `Vector`.

    // Os próximos métodos que iremos definir servem para podermos acessar
    // os valores internos do vetor. Não queremos deixar os membros públicos,
    // por diversos motivos, o mais simples e direto deles é que não queremos
    // que eles possam ser alterados diretamente por algum código de fora da
    // classe. Para isso, fazemos o que chamamos de "accessors" ou "getters".

    double x() const;
    double y() const;

    // Os métodos acima serão responsáveis apenas por retornar os valores de x
    // e y, respectivamente. Utilizando eles é impossível alterar o valor
    // interno de um vetor. Como esses métodos são incapazes de alterar o valor
    // do objeto `Vector`,  eles são declarados `const`. Isso permite que eles
    // sejam chamados quando temos objetos `Vector` constantes, ou via
    // referências `const` a `Vector`.

    // Adiante declararemos duas formas de inverter um vetor (trocando o sinal
    // de x e y). Na primeira, o vetor original não será alterado, e será criado
    // um segundo vetor representando o resultado. Na segunda, o vetor será
    // alterado de maneira "inplace". A segunda maneira poderia ter retorno
    // `void`, mas veremos o motivo de retornar `Vector&` adiante. Note a
    // referência no retorno.

    Vector &invert();

    // Os próximos métodos serão implementações "inplace" da soma de vetores
    // e da multiplicação por escalar, e serão melhor descritos na
    // implementação.

    Vector &multiply(double);
    Vector &sum(const Vector &);

    // Por fim, temos o construtor de cópia de vector. Ele não precisaria ser
    // declarado e seria criado pelo compilador. Porém, para vermos ele pela
    // primeira vez e saber a sua assinatura, temos ele declarado aqui.

    Vector(const Vector &) = default;

    // O construtor de cópia é responsável por, como o nome diz, copiar um
    // objeto. O construtor de cópia padrão que o compilador gera simplesmente
    // usa os construtores de cópia dos membros para copiá-los. Um construtor
    // de cópia também pode ser totalmente definido pelo usuário se necessário.
    // Novamente: o construtor de cópia está declarado apenas para vermos sua
    // assinatura. Se todos os membros de uma instância da classe forem
    // copiáveis, o compilador adiciona um construtor de cópia por padrão.

private:
    // Aqui temos os atributos, ou membros, do objeto. Em geral, eles são
    // mantidos na parte privada da classe, e são acessíveis apenas pelos
    // métodos dos próprios objetos. Os membros do objeto são, na maior parte
    // das vezes, o motivo de criarmos um novo tipo: agrupar informações que
    // compõem um conceito que não é diretamente representado pelos tipos
    // primitivos.

    // No tipo `Vector` temos apenas dois membros, `x_` e `y_`. Os nomes
    // possuem um underscore no fim para não conflitarem com os nomes dos
    // métodos que dão acesso (apenas de leitura) para eles.

    // Note também o valor entre `{}` depois de cada atributo. Este valor será
    // utilizado na inicialização pelo construtor padrão (explicado em detalhes
    // no apêndice [A1]). Qualquer construtor de `Vector` que não tenha outro
    // valor especificado para inicializar `x_` e `y_` utilizará os valores
    // especificados aqui.

    double x_{0}, y_{0};
};

// Por fim, fora da classe mas dentro do mesmo namespace, temos funções que
// podem ser implementadas fora da classe, por não dependerem da capacidade
// de alterar os membros ou de informações privadas da classe, pois podem
// ser implementadas com os _acessors_. Esses métodos funcionam de maneira
// semelhante às versões internas, mas em vez de alterar os objetos originais
// eles retornam um objeto novo.

Vector inverted(Vector);
Vector sum(Vector, const Vector &);
Vector multiply(Vector, double);

// As implementações dos métodos estão no arquivo vector.cpp, de maneira semelhante
// ao material da aula anterior.

}

#endif
