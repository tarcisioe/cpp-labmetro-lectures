Aula 4 - Classes
================

Chegamos à quarta aula de C++, onde finalmente aprenderemos uma das coisas mais
importantes em C++: como definir nossos próprios tipos. Para isso, temos em C++
`class`es e `struct`s. Como veremos mais adiante na aula, os dois são (quase)
sinônimos, então utilizaremos `class` durante a maior parte da aula. Várias
partes desta aula envolvem os conceitos que já vimos até agora, então se
necessário, revise o material anterior, ou consulte-o em caso de dúvida.


Seções
------

1. [Uma classe `math::Vector`](vector.h)
2. [Implementação dos métodos de `math::Vector`](vector.cpp)
3. [Utilizando a classe `math::Vector'](main_vector.cpp)
4. [Utilizando _overload_ de operadores](vector_overload.h)
5. [Implementação dos métodos com overload.](vector_overload.cpp)
6. [Utilizando a classe `math::vector'](main_vector_overload.cpp)


Exercícios
----------

1. Representar, através de classes, um aluno que possua:
    - Nome;
    - Ano de nascimento;
    - Ano de entrada no curso;
    - Três notas.

    O aluno deve também ser capaz de dizer sua idade em um ano qualquer.

2. Fazer duas funções (externas à classe) que recebam vetores de alunos,
   sendo elas:
    - Uma que retorne quem passou na disciplina (considere média 6);
    - Uma dizendo quais alunos já jubilaram, considerando:
        - O ano é 2015;
        - Não existe entrada no 2º semestre - ou seja, todos entram no 1º
          semestre;
        - O período máximo para realizar o curso é de 7 anos.

Apêndices
---------

### Construtores

Como vimos em nosso exemplo, o construtor de um objeto é responsável por sua
**inicialização**. No caso do nosso Vector, essa inicialização era simples:
bastava inicializar os valores de `x_` e `y_` do objeto.

A responsabilidade completa do construtor, na realidade, é "garantir as
invariantes do objeto", ou seja, garantir que após inicializado, o objeto
está em um estado válido e que garanta a execução correta de seus métodos.

Por enquanto, estamos trabalhando com objetos que representam apenas
coleções de dados, portanto seus construtores se responsabilizam somente por
inicializar esses dados para valores padrão, ou determinados pelo
programador.

Dos construtores de uma classe, existem 3 construtores "especiais". Estes
construtores podem ser gerados automaticamente pelo compilador, se possível.
Para um determinado tipo T, estes construtores são:

```
T() // 1
T(const T&) // 2
T(T &&) // 3
```

1. Este construtor é chamado **construtor padrão**. Ele é gerado pelo
   compilador sob as condições de que:
    * Todos os membros da classe podem ser construídos por um construtor
      padrão, ou possuem um inicializador de membro, como visto no nosso
      `Vector`.
    * O programador não declarou nenhum outro construtor diferente.

    O construtor padrão simplesmente inicializa todos os membros por seus
    respectivos construtores padrão, ou de acordo com os valores dados pelo
    inicializador de membro.

2. Este construtor se chama **construtor de cópia**. Seu objetivo é, como
   fica claro, copiar outro objeto do mesmo tipo. Este construtor é gerado
   pelo compilador sempre que todos os membros do objeto puderem ser
   construídos por cópia.

    O construtor de cópia gerado pelo compilador constrói todos os membros do
    objeto por cópia a partir dos membros respectivos do objeto.

3. Este construtor é chamado **construtor de move**. Por enquanto, como não
   temos objetos complexos responsáveis por recursos como memória, arquivos,
   conexões, etc., este construtor não tem muito sentido. Ele será explicado
   adiante.

    O construtor de move gerado pelo compilador move todos os objetos membros.
    Se um objeto não tiver um construtor de move definido por usuário, e
    nenhum membro tiver um construtor de move definido por usuário, e assim
    recursivamente, o construtor de move é equivalente ao construtor de cópia.

Para explicitamente forçar a geração de um destes construtores, podemos dizer
ao compilador que desejamos a implementação padrão, da seguinte forma.

```
T() = default;
```

Para forçar a inexistência de um dos construtores, podemos deletá-lo, da
seguinte forma:

```
T(const T&) = delete;
```

Veremos mais sobre construtores ao longo do curso, e quando começarmos a
lidar com recursos veremos o conceito simétrico: destrutores.
