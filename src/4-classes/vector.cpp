#include "vector.h"


namespace math {

Vector::Vector(double x, double y) :
    x_{x},
    y_{y}
{}

double Vector::x() const
{
    return x_;
}

double Vector::y() const
{
    return y_;
}

Vector& Vector::invert()
{
    return *this = Vector{-x_, -y_};
}

Vector &Vector::multiply(double s)
{
    return *this = {s*x_, s*y_};
}

Vector &Vector::sum(const Vector &other)
{
    x_ += other.x_;
    y_ += other.y_;

    return *this;
}

Vector inverted(Vector v)
{
    v.invert();
    return v;
}

Vector sum(Vector lhs, const Vector &rhs)
{
    lhs.sum(rhs);

    return lhs;
}

Vector multiply(Vector lhs, double s)
{
    lhs.multiply(s);

    return lhs;
}

}
