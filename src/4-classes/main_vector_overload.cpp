#include <iostream>
#include "vector_overload.h"


void print_vector(const math::Vector &v)
{
    std::cout << '{' << v.x() << ", " << v.y() << "}\n";
}


int main()
{
    using math::Vector;

    auto v = Vector{3, 5};

    print_vector(v);

    v += Vector{7, 8};

    print_vector(v);

    auto iv = -v;

    print_vector(v);
    print_vector(iv);

    v.invert();

    print_vector(v);

    v *= 0.5;

    print_vector(v);

    auto w = v + Vector{7, 8};

    print_vector(v);
    print_vector(w);

    auto u = v * 0.5;

    print_vector(v);
    print_vector(u);

    const auto &v2 = v;
}
