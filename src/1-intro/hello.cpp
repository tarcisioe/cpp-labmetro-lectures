#include <iostream>  // std::cout


int main()
{
    // Um programa simples em C++
    // --------------------------

    // Vamos iniciar com um programa bastante simples em C++, utilizando
    // números inteiros e um pouquinho de texto, e imprimindo algumas
    // informações na tela:

    int year = 2016;
    int age = 23;

    std::string name = "Tarcísio";

    std::cout << "Hello, " << name
              << ". I see you were born in "
              << year - age << "!\n";

    // Como podemos ver, começamos o programa com duas variáveis. Variáveis
    // em C++ possuem um tipo e um valor. O tipo de uma variável nunca muda,
    // mas seu valor pode mudar durante a execução do programa.

    // A variável `year` é uma variável do tipo `int`, que é utilizado para
    // representar números inteiros com sinal. Desta forma, um ano anterior
    // ao ano 0 poderia ser representado como um número negativo.

    // A variável `age`, por sua vez, representa uma idade, que não pode ser
    // negativa. Apesar disso, como é esperado que façamos operações aritméticas
    // entre `age` e valores com sinal, devemos definir `age` como um inteiro
    // com sinal mesmo assim.

    // A variável `name`, por sua vez, é de um tipo não-primitivo, ou seja,
    // um tipo definido em C++, em algum lugar da biblioteca padrão. Esse
    // é o tipo `std::string`, que utilizaremos para representar texto.

    // Após as variáveis, a linha iniciada em `std::cout` utiliza o operador
    // `<<` para escrever coisas na tela com a representação que C++ utiliza
    // da saída padrão do programa, o objeto `std::cout`.
}

