Criando um projeto no Visual Studio 2017
========================================

Para criar um projeto no Visual Studio 2017, vá em **File > New > New
Project**, e abra a seguinte janela:

<img class="figure" src="/static/images/create-project.png" height="400"/>

Com **Visual C++** selecionado, escolha a opção **Empty Project** e escreva na
parte inferior da janela o nome da Solution a ser criada ("Solution name") e o
nome do primeiro projeto dentro dela ("Name").

**Solution**: É como o Visual Studio chama a coleção de projetos, tarefas, etc.
que irão compôr uma aplicação completa.

Com o nome da solution e do primeiro projeto definidos, clique em OK e a nova
solution será aberta e aparecerá no Solution Explorer.

<img class="figure" src="/static/images/solution-explorer-1.png" height="200"/>


<img class="figure" src="/static/images/add-file.png" width="700"/>

Clicando com o botão direito em Source Files > Add > New Item... a janela
abaixo aparecerá.

<img class="figure" src="/static/images/create-main.png" height="400"/>

Escolha a opção "C++ File (.cpp)" e coloque o nome do arquivo a ser criado na
parte de baixo da janela (no campo `Name`). Só teremos um arquivo `.cpp` nesse
projeto de exemplo, e o nome dele não é importante. Para ilustrar, foi
escolhido `main.cpp`.

Depois disso, clique com o botão direito no projeto `hello` e no menu escolha a
opção "Properties". A janela abaixo irá aparecer. Em **Linker > System**, na
opção "Subsystem", escolha "Console (/SUBSYSTEM:CONSOLE)". Isso irá garantir
que o Visual Studio pause o terminal de texto após a execução para podermos ver
o que o programa imprimiu na tela.

<img class="figure" src="/static/images/set-system.png" height="400"/>

Por fim, em **C/C++ > Language**, escolha "ISO C++ 2017 Standard (/stdc++17)".
Isso irá garantir que o Visual Studio está com suporte habilitado para tudo que
o padrão mais novo da linguagem (C++17) provê.

<img class="figure" src="/static/images/cpp17.png" height="400"/>

Com isso, tudo o necessário para compilar os exemplos desta aula está
configurado. Para cada exemplo o ideal é criar um projeto novo **dentro** da
solution "Lecture1".

Para executar o código atualmente em "main.cpp", clique com o botão direito do
mouse sobre o projeto e garanta que "Set as StartUp Project" está selecionado
(o nome do projeto estará em negrito em caso positivo). Depois disso aperte F7
para compilar o projeto e na sequência Ctrl+F5 para executar.
