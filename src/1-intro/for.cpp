// O laço `for`
// ------------

#include <iostream>
#include <vector>  // std::vector

int main()
{
    // Outro caso muito comum em que precisamos executar um código
    // repetidamente é quando precisamos fazer isso um número
    // específico de vezes, ou uma vez para cada elemento em uma
    // coleção de valores. Para estes casos, os laços do tipo `for`
    // são a forma mais direta de escrever o código.

    // Em C++, temos dois tipos de laço `for`: o `for` normal
    // (clássico), e o `range-for`, introduzido em C++11. Veremos
    // os dois na sequência.

    auto grades = std::vector<double>{9.0, 8.0, 4.0};

    // Utilizaremos os dois tipos de laços `for` para calcular a média das
    // notas contidas no vetor acima.

    {
        // O primeiro laço for é a versão mais "clássica". É extremamente
        // semelhante ao laço for de C e igual ao for disponível em Java,
        // por exemplo.

        auto sum = 0.0;
        auto n = grades.size();

        for (auto i = 0u; i < n; ++i) {
            sum += grades[i];
        }

        std::cout << sum/n << "\n";

        // Como é possível ver, dentro dos parênteses do `for` existem três
        // campos, separados por `;`:
        // 1. Inicialização: Este campo executa apenas uma vez, antes do laço
        //    iniciar.
        // 2. Condição: O laço executará enquanto esta condição for verdadeira,
        //    como acontece no `while`.
        // 3. Passo: Este código executará ao final do corpo do laço, todas as
        //    vezes que o corpo também executar.

        // Qualquer uma dessas partes pode ser vazia. Se a condição for vazia, o
        // laço roda infinitamente. Por isso, podemos escrever um laço `for`
        // na forma de `while` e vice-versa.

        // Como mencionado anteriormente, o laço `for` traz uma sintaxe melhor
        // para os casos em que queremos fazer um número predefinido de passos,
        // ou passar por todos os elementos de uma coleção de valores, enquanto
        // o laço `while` descreve melhor situações em que não se sabe quantos
        // passos serão dados exatamente, e sim uma condição a ser quebrada.

        // O laço for também permite que a variável inicializada na contagem
        // sobreviva apenas durante o escopo do `for`, e seu nome pode ser
        // reutilizado depois no código.
    }

    {
        // O segundo laço é o `range-for`. Este `for` precisa de um objeto
        // sobre o qual ele possa iterar - no nosso caso, o `vector`.

        auto sum = 0.0;
        auto n = grades.size();

        for (auto i: grades) {
            sum += i;
        }

        std::cout << sum/n << "\n";

        // O `range-for` tem uma estrutura mais rígida. Sempre haverá um objeto
        // a ser iterado sobre e uma variável que irá representar, em cada
        // iteração do laço, o elemento atual do objeto.

        // No exemplo, a variável é `i`, iterando sobre o objeto `grades`.
        // No código que temos aqui, `i` será um `double`, com os valores
        // `9.0`, `8.0` e `4.0` em cada iteração do laço, respectivamente.

        // O mecanismo que permite o funcionamento do `range-for` será
        // introduzido posteriormente, quando forem explicados `iterator`s.
    }
}
