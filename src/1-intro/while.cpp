#include <iostream>  // std::cout
#include <cmath>  // sqrt


int main()
{
    // O laço `while`
    // --------------

    // Diversas vezes, ao programar, será necessário repetir alguma operação
    // diversas vezes, ou até alcançar alguma condição. Para isso utilizaremos
    // laços. Veremos 3 tipos de laços. O primeiro, e mais simples, é o laço
    // `while`. O código nesse laço irá rodar enquanto uma dada condição for
    // verdadeira.

    // A condição do `while` é dada de forma semelhante ao `if`. Ela deve ser
    // um valor booleano (`true` ou `false`). O corpo do laço executará
    // repetidamente, até que a condição deixe de ser verdadeira.

    auto old = 0.0;
    auto guess = 1.0;
    auto n = 2.0;

    while (guess != old) {
        old = guess;
        guess = (guess + n/guess)/2;
    }

    // O código acima calcula (de forma numérica) a raiz quadrada de um número
    // qualquer (`n`). Isso é feito utilizando o método de Newton [2].

    // O método de Newton se baseia em um chute inicial. Esse chute pode ser
    // maior, menor ou igual ao valor da raiz quadrada.

    // Após fazer um chute inical (arbitrariamente `1.0` aqui), é feita a média
    // do chute com n dividido pelo próprio chute. Se o chute for muito abaixo
    // da resposta real, `n/guess` será muito acima da resposta real, e a
    // média entre os dois se aproximará da resposta. O exato oposto também
    // acontece: se n for maior que a resposta correta, `n/guess` será menor
    // que a resposta e a média se aproximará do valor procurado. O resultado
    // será usado como um novo chute, e o chute anterior é registrado na
    // variável `old`.

    // Perceba a condição do while. Ele se quebra quando o chute anterior for
    // igual ao novo chute. Isso ocorrerá quando a resposta for correta, pois
    // `guess` será a raiz quadrada de n, logo `n/guess` será igual a guess,
    // e a média entre `guess` e `n/guess` também.

    // O laço `while` então faz com que executemos os passos o número de vezes
    // necessárias para que a condição se torne real.

    // No geral, o laço while é uma forma ideal de representarmos a repetição
    // de operações quando não sabemos o número necessário de repetições, e sim
    // uma condição booleana de parada.

    std::cout << guess << "\n";
    std::cout << sqrt(n) << "\n";

    return 0;
}
