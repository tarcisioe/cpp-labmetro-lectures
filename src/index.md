C++
===

Este repositório contém material de apoio para o ensino de C++.

Aulas
-----

* [Aula 1 - Primeiro projeto no Visual Studio e sintaxe básica](1-intro)
* [Aula 2 - Git e CMake](2-git-cmake)
* [Aula 3 - Funções e Namespaces](3-functions)
* [Aula 4 - Classes](4-classes)

Material de apoio
-----------------

Referências da linguagem e da biblioteca padrão podem ser encontradas
em:

* http://en.cppreference.com/w/
* http://www.cplusplus.com/

Compiladores online:

* https://ideone.com/ (C++14, apenas um .cpp)
* http://www.tutorialspoint.com/compile_cpp11_online.php
  (C++11, suporta múltiplos arquivos)
* https://wandbox.org (C++17 com GCC 7 ou Clang 5, suporta múltiplos arquivos).
* https://gcc.godbolt.org/ (Suporta diversos compiladores e permite ver o
  assembly gerado, mas não executar.)

CMake:

* [Documentação do CMake](https://cmake.org/cmake/help/latest/)
* [Documentação da Linguagem do CMake](https://cmake.org/cmake/help/latest/manual/cmake-language.7.html)

Git:

* [Pro Git v2](https://git-scm.com/book/en/v2)
* [Learn Git Branching (tutorial interativo online)](https://learngitbranching.js.org/)

Licenças
--------

Copyright (c) 2017 Tarcísio Eduardo Moreira Crocomo

* O material didático se encontra sob licença
  [CC BY-SA](https://creativecommons.org/licenses/)
* O código se encontra sob a licença [MIT](LICENSE_CODE.md)
