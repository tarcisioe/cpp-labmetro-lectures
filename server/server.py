from os import path

from flask import (abort, Flask, Markup, redirect, render_template, Response,
                   url_for)
from lxml import etree
from mistune import Markdown

from .cpp import process_cpp

markdown = Markdown(use_xhtml=True)


def first_heading(rendered):
    tree = etree.fromstring(f'<div>{rendered}</div>')

    for e in tree:
        if e.tag == 'h1':
            return e.text


def make_server(sources):
    app = Flask(__name__)

    @app.route('/<path:file>.html')
    def html(file):
        file = f'{sources}/{file}.md'

        if not path.isfile(file):
            abort(404)

        with open(file) as f:
            text = f.read()

        markup = markdown(text)

        title = first_heading(markup)
        body = Markup(markup)
        return render_template('markdown.html', title=title, body=body)

    @app.route('/raw/<path:file>.cpp')
    def raw_cpp(file):
        file = f'{sources}/{file}.cpp'

        if not path.isfile(file):
            abort(404)

        with open(file) as f:
            text = f.read()

        return Response(text, mimetype='text/plain')

    @app.route('/<path:file>.cpp')
    def cpp(file):
        file = f'{sources}/{file}.cpp'

        if not path.isfile(file):
            abort(404)

        with open(file) as f:
            text = f.read()

        body = Markup(process_cpp(text))

        return render_template('cpp.html', title=path.basename(file), body=body)

    @app.route('/', defaults={'directory': ''})
    @app.route('/<path:directory>/')
    def directory(directory):
        return(redirect(url_for('html', file=f'{directory}/index')))

    return app
