from itertools import groupby

from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import html
from mistune import Markdown


markdown = Markdown()


def process_comment(comment_block):
    lines = '\n'.join(l.strip().lstrip('//').lstrip() for l in comment_block)
    return markdown(lines)


def process_code(code_block):
    lines = '\n'.join(code_block)
    lexer = get_lexer_by_name('cpp', stripall=False)
    formatter = html.HtmlFormatter()
    return highlight(lines, lexer, formatter)


def process_cpp(cpptext: str):
    lines = cpptext.splitlines()

    groups = groupby(lines, lambda l: l.strip().startswith('//'))

    blocks = []

    for is_comment, g in groups:
        if is_comment:
            blocks.append(process_comment(g))
        else:
            blocks.append(process_code(g))

    return '\n'.join(blocks)
