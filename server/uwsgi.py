import sys

from .server import make_server


app = make_server(sys.argv[1])
