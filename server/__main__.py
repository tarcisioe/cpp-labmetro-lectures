from carl import command

from .server import make_server


@command
def main(sources):
    app = make_server(sources)

    app.run()


if __name__ == '__main__':
    main.run()
