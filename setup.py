from setuptools import setup, find_packages

VERSION = '0.1'

setup(
    name='labmetro',
    version=VERSION,
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'labmetro = server.__main__:run',
        ],
    },
    install_requires=[
        'carl',
        'flask',
        'lxml',
        'mistune',
        'pygments',
        'uwsgi',
    ],
)

